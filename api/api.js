const register_schema = require('../schema/register_schema');
const message_schema = require('../schema/message_schema');
const Promise = require('Promise');
const nodemailer = require('nodemailer');

module.exports = {
    register : function(data){
        return new Promise((reject,resolve)=>{
            register_schema.create(data,function(err,result){
                if(err)
                {
                    console.log(err);
                    reject(err);
                }
                else
                {
                    console.log(result);
                    let transporter = nodemailer.createTransport({
                        service:'Gmail',
                          secure: true, 
                          auth: {
                              user: 'kartikjain1@mrei.ac.in',
                              pass: 'justjoking'
                          }
                      });
                      let mailOptions = {
                          from : 'kartikjain1@mrei.ac.in', 
                          to : result.email, 
                          subject : 'verification code', 
                          text : 'Please verify your mail id by click this link : http://localhost:3000/verify/'+result.id
                      };
                      transporter.sendMail(mailOptions, (error, info) => {
                          if (error) {
                              return console.log("error mail",error);
                          }
                          else
                          console.log('Message %s sent: %s', info.messageId, info.response);
                      });
                    resolve('ok');
                }
            })
        })
    },

    login : function(data){
        return new Promise((reject,resolve)=>{
            register_schema.find({username : data.username, password : data.password},function(err,result){
                if(err)
                {
                    console.log(err);
                    reject(err);
                }
                else
                {
                    if(result.length > 0)
                    {
                        const loginResponse = {res : 'ok' , body : result};
                        message_schema.updateMany({to : data.username},{delivered : true},function(err,result){
                            if(err)
                            {
                                console.log(err);
                                reject(err);
                            }
                            else
                            {
                                console.log(result);
                                resolve(loginResponse);
                            }
                        })
                        
                    }
                    else
                    {
                        const loginResponse = {res : 'not_exists'};
                        
                        console.log(result);
                        resolve(loginResponse);
                    }
                }
            })
        })
    },





verify : function(data) {

    return new Promise((resolve,reject) => {
        register_schema.findOneAndUpdate({_id : data.id},{verified : true},function(err,result){
            if(err)
            {
                console.log(err);
                reject(err);
            }
            else
            {
                console.log('api',result);
                resolve('ok');
            }
        })
    })
}






}