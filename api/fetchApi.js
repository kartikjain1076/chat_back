const register_schema = require('../schema/register_schema');
const message_schema = require('../schema/message_schema');
const Promise = require('Promise');
const GroupSchema = require('../schema/groupSchema');

module.exports = {
    fetch_user : function(data){
        return new Promise((reject,resolve)=>{
            register_schema.find({},function(err,result){
                if(err)
                {
                    console.log(err);
                    reject(err);
                }
                else
                {
                    const userData = {res : 'ok' , body : result};
                    
                            console.log(result);
                            resolve(userData);
                    
                }
            })
        })
    },



fetchMessage : function(data){
    return new Promise((reject,resolve)=>{
                message_schema.updateMany({to : data.username},{delivered : true},function(err,result){
                    if(err)
                    {
                        console.log(err);
                        reject(err);
                    }
                    else
                    {
                            console.log(result);
                            message_schema.find({},function(err,result){
                            if(err)
                            {
                                console.log(err);
                                reject(err);
                            }
                            else
                            {
                                const messageData = {res : 'ok' , body : result};
                                console.log(messageData);
                                resolve(messageData);
                            }
                })
            }
        })
})
},


fetchGroup : function(data){
    return new Promise((reject,resolve)=>{
        GroupSchema.find({},function(err,result){
            if(err)
            {
                console.log(err);
                reject(err);
            }
            else
            {
                const groupData = {res : 'ok' , body : result};
                
                        console.log(result);
                        resolve(groupData);
                
            }
        })
    })
}





}