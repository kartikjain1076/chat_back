const register_schema = require('../schema/register_schema');
const message_schema = require('../schema/message_schema');
const GroupSchema = require('../schema/groupSchema');

module.exports = {
connect : function(data,cb){
    console.log('connect api',data);
    
    register_schema.findOneAndUpdate({username : data.username},{socket_id : data.socket_id , online : 'true'},function(err,result){
        if(err)
        {
            console.log(err);
            return cb(err,null)
        }
        else
        {
            console.log(result);
            return cb(null,result);
        }
})
},

disconnect : function(data,cb){
    let date = new Date();
    const day = date.getDate();
    const m = date.getMonth();
    let month = ['January','Febuary','March','April','May','June','July','August','September','October','November','December'];
    month = month[m];
    const year = date.getFullYear();
    let hours = date.getHours();
    const minutes = date.getMinutes();
    let amorpm = 'AM'
    if(hours > 12)
    {
        amorpm = 'PM';
        hours = hours - 12;
    }
    date = day + ' ' + month + ' ' + year + '        ' + hours + ':' + minutes + ' ' + amorpm;
        register_schema.findOneAndUpdate({socket_id : data.socket_id},{socket_id : null , online : date},function(err,result){
            if(err)
            {
                console.log(err);
                return cb(err,null)
            }
            else
            {
                console.log(result);
                return cb(null,result);
            }
    })
},

saveMessage : function(data,cb){
console.log('savemessage',data);
let date = new Date();
    const day = date.getDate();
    const m = date.getMonth();
    let month = ['January','Febuary','March','April','May','June','July','August','September','October','November','December'];
    month = month[m];
    const year = date.getFullYear();
    let hours = date.getHours();
    const minutes = date.getMinutes();
    let amorpm = 'AM'
    if(hours > 12)
    {
        amorpm = 'PM';
        hours = hours - 12;
    }
    date = day + ' ' + month + ' ' + year + '        ' + hours + ':' + minutes + ' ' + amorpm;
    data.time = date
message_schema.create({time : date,message:data.message,to:data.to,from:data.from,group:data.group,groupName:data.groupName},function(err,result){
    if(err)
    {
        console.log(err);
        return cb(err,null);
    }
    else
    {
        console.log(result);
        return cb(null,result);
    }
})
},


read : function(data,cb){
message_schema.updateMany({from : data.to , to : data.from},{read : true, delivered : true},function(err,result){
    if(err)
    {
        console.log(err);
        return cb(err,null);
    }
    else
    {
        console.log('read result',result);
        return cb(null,result);
    }
})
},

createGroup : function(data,cb){
GroupSchema.create(data,function(err,result){
    if(err)
    {
        console.log(err);
        return cb(err,null);
    }
    else
    {
        console.log(result);
        return cb(null,result);
    }
})
}

}