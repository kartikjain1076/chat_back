const express = require('express');
const router = express.Router();
const fetchApi = require('../api/fetchApi')


router.post('/fetch_user', async (req,res) => {
	console.log(req.body);

	try{
		let fetchUserResponse = await fetchApi.fetch_user(req.body);
		console.log('router',fetchUserResponse);
		res.send(fetchUserResponse);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
});

router.post('/fetchMessage', async (req,res) => {
	console.log(req.body);

	try{
		let fetchMessageResponse = await fetchApi.fetchMessage(req.body);
		console.log('router',fetchMessageResponse);
		res.send(fetchMessageResponse);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
});

router.post('/fetchGroup', async (req,res) => {
	console.log(req.body);

	try{
		let fetchGroupResponse = await fetchApi.fetchGroup(req.body);
		console.log('router',fetchGroupResponse);
		res.send(fetchGroupResponse);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
});




module.exports = router;