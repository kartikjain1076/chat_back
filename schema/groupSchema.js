const mongoose = require('mongoose');

const group = mongoose.Schema({
    groupName : String,
    groupUsers : Array
});

module.exports = mongoose.model('group',group);