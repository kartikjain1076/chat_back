const mongoose = require('mongoose');

const message = mongoose.Schema({
    message : String,
    to : Array,
    from : String,
    delivered : {type : Boolean , default : false},
    read : {type : Boolean , default : false},
    group : Boolean,
    groupName : String,
    time : String
});

module.exports = mongoose.model('message',message);