const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const router = require('./router/router');
const socket = require('socket.io');
const api = require('./api/socketApi');
const fetchRouter = require('./router/fetchRouter');

app.use(bodyparser.urlencoded({extended : true}));
app.use(bodyparser.json());


mongoose.connect('mongodb://127.0.0.1:27017/chat',{ useNewUrlParser: true });
app.use('/fetch',fetchRouter)
app.use('/',router);


const connection = app.listen(4000,function(){
    console.log('connected to 4000');
});

const io = socket(connection);

io.on('connection',(socket)=>{
    io.sockets.emit('user_connect','ok');
    console.log('connection made',socket.id);
    
    
    socket.on('disconnect',function(){
        console.log('disconnected : ',socket.id);
        api.disconnect({socket_id : socket.id},function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('userDisconnected',"ok");
            }
        })
    });

    socket.on('disconnectIt',function(){
        console.log('disconnected : ',socket.id);
        api.disconnect({socket_id : socket.id},function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('userDisconnected',"ok");
            }
        })
    });

    socket.on('userConnect',function(data){
        console.log('data',data);
        api.connect({socket_id : socket.id , username : data},function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('userConnected','ok');
            }
        })
    });

    socket.on('sendMessage',function(data){
        console.log('send message',data);
        api.saveMessage(data,function(err,result){
            if(err)
            {
                Console.log(err);
            }
            else
            {
                io.sockets.emit('getMessage','ok');
            }
        })
    });

    socket.on('sendRead',function(data){
        console.log('sendRead',data);
        api.read(data,function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('sentRead','ok');
            }
        })
    });

    socket.on('createGroup',function(data){
        console.log('createGroup',data);
        api.createGroup(data,function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('groupCreated','ok');
            }
        })
    });
})